##Duilib for Delphi(DDuilib)
===============================================================================

### 什么是Duilib for Delphi(DDuilib)?
***  
**Duilib for Delphi(DDuilib)是一个基于C++ [duilib](https://github.com/duilib/duilib)的开源工程，主要致力于在Delphi中使用Duilib库构建漂亮的UI。**     
**在此也非常感谢duilib作者的辛劳，没有他的库也许就没有现在这Duilib for Delphi。**  
**更多关于Duilib for Delphi的详情，可见[我的博客](http://blog.csdn.net/zyjying520/article/details/49976667)。**    

****

**自行编译duilib时请一定看ReadMe.txt中的信息**   

**欢迎加入[QQ群429151353](http://shang.qq.com/wpa/qunwpa?idkey=de0faba813de168a104d9160c9271d9873a8c91f30b416c11ff89cb2bdf6564b)一起交流，作者本人也一边写这个库，一边学习duilib。**  

***
**代码主要提交到github上，另考虑到github经常被墙和访问慢故提交一份至git.oschina上，[github项目地址](https://github.com/ying32/duilib-for-Delphi/),   [git.oschina项目地址](http://git.oschina.net/ying32/Duilib-for-Delphi)**   


### 重要说明
***
代码基于DelphiXE6编写， 也兼容Delphi7, FreePascal 。


### 目录祥情
***

* 1、 DDuilib
   * duilib for Delphi源目录。

* 2、 DuilibExport
   * 需要加入到原[duilib](https://github.com/duilib/duilib)工程中编译的c++源文件。

* 3、Demo
   * 新的Demo工程目录
   
* 4、Duilib
   * 存放原[duilib](https://github.com/duilib/duilib)工程目录，这里不提供原[duilib](https://github.com/duilib/duilib)的源码，请自行下载。
   * 4.1、 bin
      * 存放编译后的二进制及图片和xml资源，里面有的包含原duilib的资源文件  
	  
* 5、ThirdParty
   * 存放一些第三方的库或者二进制文件

### 截图
***
![截图2](https://raw.githubusercontent.com/ying32/duilib-for-Delphi/master/screenshot3.png) 
![截图1](https://raw.githubusercontent.com/ying32/duilib-for-Delphi/master/screenshot1.png)  
![截图2](https://raw.githubusercontent.com/ying32/duilib-for-Delphi/master/screenshot2.png)  


### 作者信息
***
[ying32](mailto:1444386932@qq.com) 
[QQ群429151353](http://shang.qq.com/wpa/qunwpa?idkey=de0faba813de168a104d9160c9271d9873a8c91f30b416c11ff89cb2bdf6564b) 
